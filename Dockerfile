FROM openjdk:17
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} application.jar
COPY newrelic.jar /app/newrelic.jar
COPY newrelic.yml /app/newrelic.yml
ENV NEW_RELIC_APP_NAME="New Relic Application Name"
ENV NEW_RELIC_LICENSE_KEY="New Relic License Key"
ENV NEW_RELIC_LOG_FILE_NAME="STDOUT"
EXPOSE 8080
ENTRYPOINT ["java","-javaagent:/app/newrelic.jar","-jar","/application.jar"]
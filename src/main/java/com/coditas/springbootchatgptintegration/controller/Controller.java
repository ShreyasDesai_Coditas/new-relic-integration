package com.coditas.springbootchatgptintegration.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/new-relic")
@Slf4j
public class Controller {
    @PostMapping
    public ResponseEntity<Void> postMappingWithLog() throws InterruptedException {
        log.info("[New Relic] This is a new log");
        Thread.sleep(2000L);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping
    public void throwError(){
        throw new RuntimeException("This is an error message");
    }
}

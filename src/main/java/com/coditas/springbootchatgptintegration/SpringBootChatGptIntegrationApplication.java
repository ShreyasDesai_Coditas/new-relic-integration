package com.coditas.springbootchatgptintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootChatGptIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootChatGptIntegrationApplication.class, args);
	}

}
